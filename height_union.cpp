#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Node{
    public:
        int id=-200;
        int id_parent=-200;
        int heigh=-200;
        bool is_used=false;
    private:

};

int collapsingfind(int a,vector <vector <Node>> &b){
    //find element a is in which tree
    int leave=0;
    int i=0;
    int j=0;
    int count=0;

    for(i=0;b.size();i++){
        if(b[i][0].is_used==false)continue;
        for(j=0;j<b[i].size();j++){
            if(b[i][j].id==a){
                leave=1;
                break;
            }
        }
        if(leave==1)break;
    }

    //cout<<b[i][j].id_parent;

    //collapse the tree , set the element on the path from a to a`s root as childern of a`s root
    while(1){
        if(b[i][j].id_parent==b[i][0].id){
            count++;
            break;
        }
        int cur_id=b[i][j].id;
        int cur_id_parent=b[i][j].id_parent;

        b[i][j].id_parent=b[i][0].id;
        count++;
        //cout<<b[i][j].id<<endl<<cur_id_parent<<endl;

        for(j=0;j<b[i].size();j++){
            if(b[i][j].id==cur_id_parent){
                count++;
                break;
            }
        }
    }
    cout<<count<<endl;
}

void heightunion(int a,int b,vector <vector <Node>> &c){

    //compare which tree is higher
    int height1=0;
    int height2=0;
    int tree1=0;
    int tree2=0;
    for(int i=0;i<c.size();i++){
        if(c[i][0].is_used==false)continue;
        if(c[i][0].id==a){
            tree1=i;
            height1=c[i][0].heigh;
        }
        if(c[i][0].id==b){
            tree2=i;
            height2=c[i][0].heigh;
        }
    }

    //if tree2 is not higher than tree1,set tree2 as a subtree to tree1 
    if(height1>=height2){
        c[tree2][0].id_parent=c[tree1][0].id;
        for(int i=0;i<c[tree2].size();i++){
            c[tree1].push_back(c[tree2][i]);
        }
        c[tree1][0].heigh+=c[tree2][0].heigh;
        c[tree2][0].is_used=false;
    }

    //if tree2 is higher than tree1,set tree1 as a subtree to tree2
    if(height1<height2){
        c[tree1][0].id_parent=c[tree2][0].id;
        for(int i=0;i<c[tree1].size();i++){
            c[tree2].push_back(c[tree1][i]);
        }
        c[tree2][0].heigh+=c[tree1][0].heigh;
        c[tree1][0].is_used=false;
    }
}

int count_node(int a,vector <vector <Node>> &b){    //get the node number of the subtree which root is a
    
    //find element a is in which tree
    int leave=0;
    int count=0;
    int i=0;
    int j=0;
    for(i=0;b.size();i++){
        for(j=0;j<b[i].size();j++){
            if(b[i][j].id==a){
                leave=1;
                break;
            }
        }
        if(leave==1)break;
    }

    //search the subtree
    for(int k=0;k<b[i].size();k++){
        if(b[i][k].id_parent==a){
            count++;
            count+=count_node(b[i][k].id,b);
        }
    }
    return count;
}

int main(){
    int tree_num=0;
    cin>>tree_num;

    int id=0;
    int id_parent=0;
    int *node_num=new int[tree_num];   //an array to store every tree`s node number
    string act="";  //FIND STOP UNION
    vector <vector <Node>>tree;

    Node node_0;
    int count=0;

    for(int i=0;i<tree_num;i++){
        cin>>node_num[i];
        
        vector<Node>subtree(node_num[i],node_0);
        tree.push_back(subtree);
        //tree[i]=new Node[node_num[i]];
        for(int j=0;j<node_num[i];j++){
            //tree[i].push_back(node_0);
            cin>>id>>id_parent;
            if(id_parent<0){
                tree[i][0].id=id;
                tree[i][0].id_parent=id_parent;
                tree[i][0].heigh=-id_parent;
                tree[i][0].is_used=true;
            }
            else{
                tree[i][j].id=id;
                tree[i][j].id_parent=id_parent;
            }
        }
    }

    
    while(act!="STOP"){
        int input1=0;
        int input2=0;
        cin>>act;
        if(act=="UNION"){
            cin>>input1>>input2;
            heightunion(input1,input2,tree);
        }
        if(act=="FIND"){
            cin>>input1;
            collapsingfind(input1,tree);
        }
    }
    
    //cout<<count_node(3,tree);
    //for(int i=0;i<tree_num;i++)delete(node[i]);
    delete(node_num);

    return 0;
}