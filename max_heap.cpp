#include<iostream>
#include<string>

using namespace std;

//keep heap always a max_heap
void reorder(int *heap,int size){
    int num=1;
    int tmp;
    while(num<=size){
        if(num*2>size)break;
        //compare with left child
        if(heap[num-1]<heap[2*num-1]){
            tmp=heap[num-1];
            heap[num-1]=heap[2*num-1];
            heap[2*num-1]=tmp;
            num=2*num;
        }
        else{
            //compare with right child
            if(heap[num-1]<heap[2*num]){
                tmp=heap[num-1];
                heap[num-1]=heap[2*num];
                heap[2*num]=tmp;
                num=2*num+1;
            }
            //if parent`s value is larger than both child`s,do nothing
            if(heap[num]>=heap[2*num+1])break;
        }
    }
}

int main(){
    int M=0;    //row number
    int N=0;    //number of fans
    int test=0;
    cin>>test;

    int price[test]={0};

    for(int i=0;i<test;i++){
        cin>>M>>N;
        int X[M]={0};   //number of empty seat in n-th row
        int *heap=new int[M];
        
        for(int j=0;j<M;j++){
            cin>>X[j];
        }
        
        heap[0]=X[0];
        for(int j=1;j<M;j++){
            int k=0;
            while(heap[k]>X[j])k++; //find the place to insert
            for(int a=M-1;a>k;a--){
                heap[a]=heap[a-1];  //put the element which after the insert point one place back
            }
            heap[k]=X[j];
        }
        for(int j=0;j<N;j++){
            price[i]+=heap[0];
            heap[0]=heap[0]-1;
            reorder(heap,M);
        }
        delete(heap);
    }

    for(int i=0;i<test;i++)cout<<price[i]<<endl;    //print result

    return 0;
}